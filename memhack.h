#pragma once
#include <Windows.h>
#include <iostream>

//internal patch
void Patch(char* dst, char* src, unsigned int size);

//external
void PatchEx(char* dst, char* src, unsigned int size, HANDLE hProcess);

//Internal Nop
void Nop(char* dst, unsigned int size);

//External
void NopEx(char* dst, unsigned int size, HANDLE hProcess);


uintptr_t FindDMAAddy(HANDLE hProcHandle, uintptr_t baseAddress, uintptr_t offsets[], size_t numOffsets);

uintptr_t FindDMAAddyVA(HANDLE hProcHandle, uintptr_t pointer, size_t numOffsets, ...);