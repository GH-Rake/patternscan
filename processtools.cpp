#include "processtools.h"

typedef NTSTATUS(__stdcall* tNtQueryInformationProcess)
(
	HANDLE ProcessHandle,
	PROCESSINFOCLASS ProcessInformationClass,
	PVOID ProcessInformation,
	ULONG ProcessInformationLength,
	PULONG ReturnLength
);

tNtQueryInformationProcess NtQueryInfoProc = nullptr;

bool ImportNTQueryInfo()
{
	NtQueryInfoProc = (tNtQueryInformationProcess)GetProcAddress(GetModuleHandle(TEXT("ntdll.dll")), "NtQueryInformationProcess");
	if (NtQueryInfoProc == nullptr)
	{
		return false;
	}
	else return true;
}

//Get Process by name
bool Process::Get()
{
	PROCESSENTRY32 procEntry = { 0 };
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (hSnapshot)
	{
		procEntry.dwSize = sizeof(PROCESSENTRY32);

		if (Process32First(hSnapshot, &procEntry))
		{
			do
			{
				if (!_tcscmp(procEntry.szExeFile, this->name))
				{
					bFound = true;
					memcpy(this, &procEntry, sizeof(procEntry));
					CloseHandle(hSnapshot);

					return true;
				}
			} while (Process32Next(hSnapshot, &procEntry));
		}
	}
	CloseHandle(hSnapshot);
	return false;
}

Process::Process(TCHAR* exeName)
{
	this->name = exeName;
	Get();
}

bool Process::Attach()
{
	handle = OpenProcess(PROCESS_ALL_ACCESS | PROCESS_QUERY_INFORMATION, NULL, th32ProcessID);

	if (handle)
	{
		return true;
	}
	else return false;
}


void Process::GetBit()
{
	SYSTEM_INFO si;
	GetNativeSystemInfo(&si);
	if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
	{
		BOOL tempWow64 = FALSE;
		IsWow64Process(handle, &tempWow64); //EXTERNAL

		if (tempWow64 == TRUE)
		{
			bWow64 = true;
			b32bit = true;
		}

		else
		{
			bWow64 = false;
			b32bit = false;
		}
	}

	else
	{
		b32bit = true;
		bWow64 = false;
	}
}


//External
bool Process::GetPEB()
{
	GetBit();

	if (!NtQueryInfoProc)
	{
		ImportNTQueryInfo();
	}

	if (NtQueryInfoProc)
	{
		NTSTATUS status = NtQueryInfoProc(handle, ProcessBasicInformation, &pbi, sizeof(pbi), 0);
		if (NT_SUCCESS(status))
		{
			ReadProcessMemory(handle, pbi.PebBaseAddress, &peb, sizeof(peb), 0);
			return true;
		}
	}

	return false;
}

/*
//internal
void Process::GetPEB()
{
	//32bit
	peb = (PEB*)__readfsdword(0x30);

	//64bit
	//peb = (PEB*)__readfsqword(0x60);
}
*/

Module::Module(Process* process, TCHAR* moduleName)
{
	this->name = moduleName;
	this->process = process;
	Get();
}

bool Module::Get()
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, process->th32ProcessID);

	MODULEENTRY32 modEntry;

	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		modEntry.dwSize = sizeof(MODULEENTRY32);

		if (Module32First(hSnapshot, &modEntry))
		{
			do
			{
				if (!_tcscmp(modEntry.szModule, name))
				{
					bFound = true;
					memcpy(this, &modEntry, sizeof(modEntry));					
					CloseHandle(hSnapshot);
					return true;
				}
			} while (Module32Next(hSnapshot, &modEntry));
		}
		CloseHandle(hSnapshot);
	}
	bFound = false;
	return false;
}

//internal, still needs testing
bool Module::GetLDREntry()
{
	/*
#ifndef _UNICODE
	wchar_t wc_modulename = TO_WCHAR_T(szExePath);
#endif
	
	if (!process->peb)
	{
		process->GetPEB();
	}

	LIST_ENTRY& links = process->peb->Ldr->InMemoryOrderModuleList;

	_LIST_ENTRY head = links;

	_LIST_ENTRY moduleData = *head.Flink;

	while (moduleData.Flink)
	{
		LDR_DATA_TABLE_ENTRY * mod = (LDR_DATA_TABLE_ENTRY*)moduleData.Flink;

		#ifndef _UNICODE
		if (mod->FullDllName.Buffer == wc_modulename)
		#else
		if (mod->FullDllName.Buffer == szExePath)
		#endif
		{
			ldr = *mod;
			return true;
		}
		moduleData = *moduleData.Flink;
	}
	*/
	return false;
	
}