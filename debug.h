#pragma once
#include <windows.h>
#include "processtools.h"

namespace Debug
{
	//External check PEB for IsDebugged flag
	bool IsDebugFlagSet(Process* process);

	//External set PEB IsDebugged flag false
	bool SetDebugFlagFalse(Process* process);

	//Make IsDebuggerPresent return false with WPM
	bool PatchIsDebuggerPresent(Process* process);

	//Place a hook on IsDebuggerPresent to return false
	bool HookIsDebuggerPresent(Process* process);//

	//Snippet to showcase debug functionality
	void TutorialTestPatchDebugger();
}