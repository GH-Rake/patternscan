#include "memhack.h"

//internal patch
void Patch(char* dst, char* src, unsigned int size)
{
	DWORD oldprotect;
	VirtualProtect(dst, size, PAGE_READWRITE, &oldprotect);
	memcpy(dst, src, size);
	VirtualProtect(dst, size, oldprotect, &oldprotect);
}

//external
void PatchEx(char* dst, char* src, unsigned int size, HANDLE hProcess)
{
	DWORD oldprotect;
	VirtualProtectEx(hProcess, dst, size, PAGE_READWRITE, &oldprotect);
	WriteProcessMemory(hProcess, dst, src, size, NULL);
	VirtualProtectEx(hProcess, dst, size, oldprotect, &oldprotect);
}

//Internal Nop
void Nop(char* dst, unsigned int size)
{
	DWORD oldprotect;
	VirtualProtect(dst, size, PAGE_READWRITE, &oldprotect);
	memset(dst, 0x90, size);
	VirtualProtect(dst, size, oldprotect, &oldprotect);
}

//External
void NopEx(char* dst, unsigned int size, HANDLE hProcess)
{
	byte* nopArray = new byte[size];
	memset(nopArray, 0x90, size);

	DWORD oldprotect;
	VirtualProtectEx(hProcess, dst, size, PAGE_READWRITE, &oldprotect);
	WriteProcessMemory(hProcess, dst, nopArray, size, NULL);
	VirtualProtectEx(hProcess, dst, size, oldprotect, &oldprotect);
	delete[] nopArray;
}

uintptr_t FindDMAAddy(HANDLE hProcHandle, uintptr_t pointer, uintptr_t offsets[], size_t numOffsets)
{
	//std::cout << "BaseAddress    : 0x" << std::hex << pointer << std::endl << std::endl;

	//Dereference pointer by reading the value of the pointer
	if (!ReadProcessMemory(hProcHandle, (LPCVOID)pointer, &pointer, sizeof(pointer), nullptr))
	{
		return 0; //if RPM fails
	}

	//For each offset
	for (size_t i = 0; i < numOffsets; i++)
	{
		//std::cout << "Points to      : 0x" << std::hex << pointer << std::endl << std::endl;
		//std::cout << "+offset 0x" << std::hex << offsets[i] << "  : 0x" << std::hex << pointer + offsets[i] << std::endl << std::endl;

		//Add offset and dereference that address
		if (!ReadProcessMemory(hProcHandle, (LPVOID)(pointer + offsets[i]), &pointer, sizeof(pointer), nullptr))
		{
			return 0; //if RPM fails
		}
	}

	//std::cout << "Final Address  : 0x" << std::hex << pointer << std::endl << std::endl;
	//int value;
	//ReadProcessMemory(hProcHandle, (void*)pointer, &value, sizeof(value), NULL);
	//std::cout << "Final Value    : 0x" << std::hex << value << " or decimal " << std::dec << value << std::endl << std::endl;

	return pointer; //return address pointed to by pointer
}

uintptr_t FindDMAAddyVA(HANDLE hProcHandle, uintptr_t pointer, size_t numOffsets, ...)
{
	va_list args; //create argument list to store arguments
	va_start(args, numOffsets); //populate argument "args" with all arguments after numOffsets

	//std::cout << "BaseAddress    : 0x" << std::hex << pointer << std::endl << std::endl;

	//Dereference pointer by reading the value of the pointer
	if (!ReadProcessMemory(hProcHandle, (LPCVOID)pointer, &pointer, sizeof(pointer), NULL))
	{
		return 0; //if RPM fails
	}

	//For each offset
	for (size_t i = 0; i < numOffsets; i++)
	{
		uintptr_t offset = va_arg(args, uintptr_t);
		//std::cout << "Points to      : 0x" << std::hex << pointer << std::endl << std::endl;
		//std::cout << "+offset 0x" << std::hex << offset << "  : 0x" << std::hex << pointer + offset << std::endl << std::endl;

		//Add offset and dereference that address
		if (!ReadProcessMemory(hProcHandle, (LPVOID)(pointer + offset), &pointer, sizeof(pointer), nullptr))
		{
			return 0;//if RPM fails
		}
	}

	//std::cout << "Final Address  : 0x" << std::hex << pointer << std::endl << std::endl;
	//int value;
	//ReadProcessMemory(hProcHandle, (void*)pointer, &value, sizeof(value), NULL);
	//std::cout << "Final Value    : 0x" << std::hex << value << " or decimal " << std::dec << value << std::endl << std::endl;

	va_end(args); //cleanup after va_list
	return pointer; //return address pointed to by pointer
}