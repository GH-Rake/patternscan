#include <Windows.h>
#include "process.h"
#include "patternscan.h"
#include "memhack.h"
#include "stringconvert.h"
#include "debug.h"

int main()
{
	//String Conversion Snippet for tutorial
	StringConversions();

	//Get procEntry
	Process acProc = Process(TEXT("ac_client.exe"));

	//exit if process not found
	if (!acProc.bFound) return 1;

	//Get handle by OpenProcess
	acProc.Attach();

	//Get modEntry
	Module acMod = Module(&acProc, TEXT("ac_client.exe"));

	/*
	//External Pattern Scanning
	if (acMod.bFound)
	{
		//PatternScan for pattern in ac_client.exe module of ac_client.exe process
		char* healthDecAddress = Pattern::Ex::Mod("\x29\x7b\x00\x8b\xc7", "xx?xx", &acMod);

		//Scan module using combo pattern
		healthDecAddress = Pattern::Ex::Mod("29 7b ? 8b c7", &acMod);

		//Scan all modules using combo pattern
		healthDecAddress = Pattern::Ex::AllMods("29 7b ? 8b c7", &acProc);

		//Scan all comitted memory of process
		healthDecAddress = Pattern::Ex::Proc("29 7b ?? 8b c7", &acProc);
		
		//Nop the instructions
		if (healthDecAddress)
		{
		//	NopEx(healthDecAddress, 5, acProc.handle);
		}
	}
	*/
	
	/*
	unsigned char u = '\xFF';
	char c = '\xFF';

	if (u == c)
	{
		char* YouAre = "USING /J FAGGOT";
	}

	*/

	//char* good = Pattern::Ex::Mod("FF ??", &acMod);
	//char* good2 = Pattern::Ex::Scan("\x96\x96\x6d\x6f", "xxxx", (char*)0x02A20438, (char*)(0x02A20438 + 0x99999), &acProc); //this doesn't work with reg char eh?
	
	uintptr_t pLocalPlayer = 0x050F4F4;
	uintptr_t ammoOffsets[] = { 0x374, 0x14 };
	uintptr_t addr = FindDMAAddy(acProc.handle, 0x050F4F4, ammoOffsets, 2);//do variardic arguments faggot

	std::cout << std::endl << std::endl;


	uintptr_t addr2 = FindDMAAddyVA(acProc.handle, 0x050f4f4, 2, 0x374, 0x14);


	char* test = Pattern::Ex::Proc("?? ?? 80 cb 52 00 ?? 74 ?? e9", &acProc);
	

	//Internal Pattern Scanning

	//here's our pattern we will scan for:
	char* kewlPattern = "\x13\x37\xC0\xDE\x13\x19\x17\x14";

	Process thisapp = Process(TEXT("PatternScanExTutorial.exe"));

	thisapp.Attach();

	Pattern::Ex::Proc("13 37 C0 DE 13 19 17 14", &thisapp);

	return 0;
}