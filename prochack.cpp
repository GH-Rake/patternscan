#include "prochack.h"
#include "FW_NT.h"

typedef NTSTATUS(__stdcall* tNtQueryInformationProcess)
(
	HANDLE ProcessHandle,
	PROCESSINFOCLASS ProcessInformationClass,
	PVOID ProcessInformation,
	ULONG ProcessInformationLength,
	PULONG ReturnLength
	);

tNtQueryInformationProcess NtQueryInfoProc = nullptr;

bool ImportNTQueryInfo()
{
	NtQueryInfoProc = (tNtQueryInformationProcess)GetProcAddress(GetModuleHandle(TEXT("ntdll.dll")), "NtQueryInformationProcess");
	if (NtQueryInfoProc == nullptr)
	{
		return false;
	}
	else return true;
}

//Get ProcessEx by name
bool ProcessEx::Get()
{
	PROCESSENTRY32 procEntry = { 0 };
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (hSnapshot)
	{
		procEntry.dwSize = sizeof(PROCESSENTRY32);

		if (Process32First(hSnapshot, &procEntry))
		{
			do
			{
				if (!_tcscmp(procEntry.szExeFile, this->name))
				{
					bFound = true;
					memcpy(this, &procEntry, sizeof(procEntry));
					CloseHandle(hSnapshot);

					return true;
				}
			} while (Process32Next(hSnapshot, &procEntry));
		}
	}
	CloseHandle(hSnapshot);
	return false;
}

ProcessEx::ProcessEx(TCHAR* exeName)
{
	this->name = exeName;
	Get();
}

bool ProcessEx::Attach()
{
	handle = OpenProcess(PROCESS_ALL_ACCESS | PROCESS_QUERY_INFORMATION, NULL, th32ProcessID);

	if (handle)
	{
		return true;
	}
	else return false;
}

void ProcessEx::GetBit()
{
	SYSTEM_INFO si;
	GetNativeSystemInfo(&si);
	if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
	{
		BOOL tempWow64 = FALSE;
		IsWow64Process(handle, &tempWow64); //EXTERNAL

		if (tempWow64 == TRUE)
		{
			bWow64 = true;
			b32bit = true;
		}

		else
		{
			bWow64 = false;
			b32bit = false;
		}
	}

	else
	{
		b32bit = true;
		bWow64 = false;
	}
}

//External
bool ProcessEx::GetPEB()
{
	GetBit();

	if (!NtQueryInfoProc)
	{
		ImportNTQueryInfo();
	}

	if (NtQueryInfoProc)
	{
		NTSTATUS status = NtQueryInfoProc(handle, ProcessBasicInformation, &pbi, sizeof(pbi), 0);
		if (NT_SUCCESS(status))
		{
			ReadProcessMemory(handle, pbi.PebBaseAddress, &peb, sizeof(peb), 0);
			return true;
		}
	}

	return false;
}

//internal
bool ProcessIn::GetPEB()
{
#ifdef _WIN64
	//64bit
	peb = (PEB*)__readfsqword(0x60);

#else
	//32bit
	peb = (PEB*)__readfsdword(0x30);
#endif

	return peb != nullptr;
}

bool ProcessIn::GetBit()
{
#ifdef _WIN64
	//64bit
	b32Bit = false;
	bWow64 = false;

#else
	//32bit
	b32bit = true;
	SYSTEM_INFO si;
	GetNativeSystemInfo(&si);
	if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
	{
		bWow64 = true;
	}
#endif

	return true;///how to include error checking?
}

Module::Module(ProcessEx* process, TCHAR* moduleName)
{
	this->name = moduleName;
	this->process = process;
	Get();
}

bool Module::Get()
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, process->th32ProcessID);

	MODULEENTRY32 modEntry;

	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		modEntry.dwSize = sizeof(MODULEENTRY32);

		if (Module32First(hSnapshot, &modEntry))
		{
			do
			{
				if (!_tcscmp(modEntry.szModule, name))
				{
					bFound = true;
					memcpy(this, &modEntry, sizeof(modEntry));
					CloseHandle(hSnapshot);
					return true;
				}
			} while (Module32Next(hSnapshot, &modEntry));
		}
		CloseHandle(hSnapshot);
	}
	bFound = false;
	return false;
}

//internal - Passed all tests g00d to g0!
bool Module::GetLDREntryIn()
{
	processin->GetPEB();

	/*
	//this works for test but need to change InMemoryOrderModuleList to be a FW_NT_LDR_DATA_TABLE_ENTRY and stop using winternl.h
	LIST_ENTRY test;
	test.list = (FW_NT_LDR_DATA_TABLE_ENTRY)processin->peb->Ldr->InMemoryOrderModuleList;
	//
	*/

	LIST_ENTRY head = processin->peb->Ldr->InMemoryOrderModuleList;

	//LIST_ENTRY moduleData = *head.Flink;
	LIST_ENTRY curr = head;

	while (curr.Flink != head.Blink)
	{
		LDR_DATA_TABLE_ENTRY * mod = (LDR_DATA_TABLE_ENTRY*)curr.Flink;

		if (mod->FullDllName.Buffer)
		{
			if (wcscmp(mod->FullDllName.Buffer, szModule) == 0)
			{
				ldr = *mod;
				return true;
			}
		}
		curr = *curr.Flink;
	}
	return false;
}

//thanks @mem and @mambda //Change class member function and argument to match
//Get module from PEB, bypassing ToolHelp32Snapshot if it's blocked/detected somehow
//Tested good so far
bool ProcessEx::GetModuleFromPEB(wchar_t* moduleName)
{
	GetPEB();

	PEB_LDR_DATA ldrData;
	SIZE_T bytesRead;
	ReadProcessMemory(handle, peb.Ldr, &ldrData, sizeof(ldrData), &bytesRead);
	auto head = ldrData.InMemoryOrderModuleList.Flink;
	auto cur = head;
	do
	{
		FW_LDR_MODULE te = {};

		auto readAddr = CONTAINING_RECORD(cur, LDR_DATA_TABLE_ENTRY, InMemoryOrderLinks);
		ReadProcessMemory(handle, readAddr, &te, sizeof(te), &bytesRead);

		if (te.BaseDllName.Length)
		{
			wchar_t * buf = new wchar_t[te.BaseDllName.Length + 2];
			ReadProcessMemory(handle, te.BaseDllName.Buffer, buf, te.BaseDllName.Length + 2, &bytesRead);

			if (wcscmp(buf, moduleName) == 0)
			{
				//return result here
				delete buf;
				return true;
			}
			delete buf;
		}
		cur = te.InMemoryOrderModuleList.Flink;
	} while (cur != head); //Does this break from loop when module is not found? tested good so far

	return false;
}

struct llitem
{
	uintptr_t flink;
	uintptr_t blink;

	void Get(HANDLE handle, LIST_ENTRY* item)
	{
		ReadProcessMemory(handle, item, this, sizeof(LIST_ENTRY), 0);
	}
};

//https://www.codeproject.com/articles/320748/haephrati-elevating-during-runtime
BOOL IsAppRunningAsAdminMode()
{
	BOOL fIsRunAsAdmin = FALSE;
	DWORD dwError = ERROR_SUCCESS;
	PSID pAdministratorsGroup = NULL;

	// Allocate and initialize a SID of the administrators group.
	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	if (!AllocateAndInitializeSid(
		&NtAuthority,
		2,
		SECURITY_BUILTIN_DOMAIN_RID,
		DOMAIN_ALIAS_RID_ADMINS,
		0, 0, 0, 0, 0, 0,
		&pAdministratorsGroup))
	{
		dwError = GetLastError();
		goto Cleanup;
	}

	// Determine whether the SID of administrators group is enabled in
	// the primary access token of the process.
	if (!CheckTokenMembership(NULL, pAdministratorsGroup, &fIsRunAsAdmin))
	{
		dwError = GetLastError();
		goto Cleanup;
	}

Cleanup:
	// Centralized cleanup for all allocated resources.
	if (pAdministratorsGroup)
	{
		FreeSid(pAdministratorsGroup);
		pAdministratorsGroup = NULL;
	}

	// Throw the error if something failed in the function.
	if (ERROR_SUCCESS != dwError)
	{
		throw dwError;
	}

	return fIsRunAsAdmin;
}

void Elevate()
{
	wchar_t szPath[MAX_PATH];
	if (GetModuleFileName(NULL, szPath, ARRAYSIZE(szPath)))
	{
		// Launch itself as admin
		SHELLEXECUTEINFO sei = { sizeof(sei) };
		sei.lpVerb = L"runas";
		sei.lpFile = szPath;
		sei.hwnd = NULL;
		sei.nShow = SW_NORMAL;
		if (!ShellExecuteEx(&sei))
		{
			DWORD dwError = GetLastError();
			if (dwError == ERROR_CANCELLED)
			{
				// The user refused to allow privileges elevation.
				//std::cout << "User did not allow elevation" << std::endl;
			}
		}
		else
		{
			_exit(1);  // Quit itself
		}
	}
}