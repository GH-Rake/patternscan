#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include <Winternl.h>
#include <stdlib.h>
#include <tchar.h>
#include <vector>
#include "memhack.h"

class Module;

class Process : public PROCESSENTRY32
{
public:
	TCHAR* name;
	HANDLE handle = 0;
	bool bFound = false;

	bool b32bit = false;
	bool bWow64 = false;

	PROCESS_BASIC_INFORMATION pbi;
	_PEB peb;

	//Constructor
	Process(TCHAR* exeName);

	//Finds process and get the procEntry
	bool Get();

	//Gets Handle to process
	bool Attach();

	//Defines process as 32/64 bit
	void GetBit();

	//Use NTQueryInformation to get the PEB address and make a copy of it
	bool GetPEB();
};

class Module : public MODULEENTRY32
{
public:
	TCHAR* name;
	bool bFound = false;
	Process * process; //pointer to parent process

	LDR_DATA_TABLE_ENTRY ldr; //?

	//Constructor
	Module(Process* process, TCHAR* moduleName);

	//Find module and get the modEntry
	bool Get();


	bool GetLDREntry();
};