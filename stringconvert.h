#pragma once

//Convert char* to wchar_t*
wchar_t* TO_WCHAR_T(char* string)
{
	size_t len = strlen(string) + 1;
	wchar_t* wc_string = new wchar_t[len];
	size_t numCharsRead;
	mbstowcs_s(&numCharsRead, wc_string, len, string, _TRUNCATE);
	return wc_string;
}

//Convert wchar_t* to char*
char* TO_CHAR(wchar_t* string)
{
	size_t len = wcslen(string) + 1;
	char* c_string = new char[len];
	size_t numCharsRead;
	wcstombs_s(&numCharsRead, c_string, len, string, _TRUNCATE);
	return c_string;
}

//String Conversion Snippet
void StringConversions()
{
	char* c_string = "ac_client.exe";
	wchar_t* wc_converted = TO_WCHAR_T(c_string);
	delete wc_converted;

	wchar_t * wc_string = L"ac_client.exe";
	char * c_converted = TO_CHAR(wc_string);
	delete c_converted;
}