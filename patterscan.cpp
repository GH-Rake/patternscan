#include "patternscan.h"

//Split combo pattern into mask/pattern
void Pattern::Parse(char* combo, char* pattern, char* mask)
{
	unsigned int patternLen = (strlen(combo) + 1) / 3;

	for (unsigned int i = 0; i < strlen(combo); i++)
	{
		if (combo[i] == ' ')
		{
			continue;
		}

		else if (combo[i] == '?')
		{
			if ((WORD)combo[i] == '??')
			{
				mask[(i + 1) / 3] = '?';
				i += 2;
			}
			else
			{
				mask[(i + 1) / 3] = '?';
				i += 1;
			}
		}

		else
		{
			char byte = (char)strtol(&combo[i], 0, 16);
			pattern[(i + 1) / 3] = byte;
			mask[(i + 1) / 3] = 'x';
			i += 2;
		}
	}
	pattern[patternLen] = '\0';
	mask[patternLen] = '\0';
}

//Internal Pattern Scan
char* Pattern::In::Scan(char* pattern, char* mask, char* begin, unsigned int size)
{
	//strlen the mask, not the pattern if you use the pattern
	//you will get short length because null terminator
	unsigned int patternLength = strlen(mask);

	for (unsigned int i = 0; i < size - patternLength; i++)
	{
		bool found = true;
		for (unsigned int j = 0; j < patternLength; j++)
		{
			if (mask[j] != '?' && pattern[j] != *(begin + i + j))
			{
				found = false;
				break;
			}
		}
		if (found)
		{
			return (begin + i);
		}
	}
	return nullptr;
}

char* Pattern::In::Mod(char *combopattern, Module* module)
{
	//placeholder
	module->process->GetPEB(); //this is external function dumbass
	return nullptr;
}

char* Pattern::In::AllMods(char* combopattern)
{
	// placeholder
	return nullptr;
}

char* Pattern::In::Proc(char* combopattern)
{
	// placeholder
	return nullptr;
}

//External Wrapper
char* Pattern::Ex::Scan(char* pattern, char* mask, char* begin, char* end, Process* process)
{
	char* currentChunk = begin;
	char* match = nullptr;
	SIZE_T bytesRead;

	while (currentChunk < end)
	{
		MEMORY_BASIC_INFORMATION mbi;
		
		//return nullptr if VirtualQuery fails
		if (!VirtualQueryEx(process->handle, currentChunk, &mbi, sizeof(mbi)))
		{
			return nullptr;
		}

		char* buffer = new char[mbi.RegionSize]; ////////bad on 64bit /////////////////////////////////////////////////
		//can move this down

		if (mbi.State == MEM_COMMIT && mbi.Protect != PAGE_NOACCESS)
		{
			DWORD oldprotect;
			if (VirtualProtectEx(process->handle, mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &oldprotect))
			{
				ReadProcessMemory(process->handle, mbi.BaseAddress, buffer, mbi.RegionSize, &bytesRead);
				VirtualProtectEx(process->handle, mbi.BaseAddress, mbi.RegionSize, oldprotect, &oldprotect);

				char* internalAddress = In::Scan(pattern, mask, buffer, bytesRead);

				if (internalAddress != nullptr)
				{
					//calculate from internal to external
					uintptr_t offsetFromBuffer = internalAddress - buffer;
					match = currentChunk + offsetFromBuffer;
					delete[] buffer;
					break;
				}
			}
		}

		currentChunk = currentChunk + mbi.RegionSize;
		delete[] buffer;
	}
	return match;
}

//Module wrapper for external pattern scan
char* Pattern::Ex::Mod(char* pattern, char* mask, Module* module)
{
	return Scan(pattern, mask, (char*)module->modBaseAddr, (char*)module->modBaseAddr + module->modBaseSize, module->process);
}

//Loops through all modules and scans them
char* Pattern::Ex::AllMods(char* pattern, char* mask, Process* process)
{
	MODULEENTRY32 modEntry = { 0 };
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, process->th32ProcessID);
	if (hSnapshot != INVALID_HANDLE_VALUE)
	{
		modEntry.dwSize = sizeof(MODULEENTRY32);
		if (Module32First(hSnapshot, &modEntry))
		{
			do
			{
				char* match = Scan(pattern, mask, (char*)modEntry.modBaseAddr, (char*)(modEntry.modBaseAddr + modEntry.modBaseSize), process);
				if (match != nullptr)
				{
					CloseHandle(hSnapshot);
					return match;
				}
			} while (Module32Next(hSnapshot, &modEntry));
		}
		CloseHandle(hSnapshot);
	}
	return nullptr;
}

//Combo Loop through all modules
char* Pattern::Ex::AllMods(char* combopattern, Process* process)
{
	unsigned int patternLen = ((strlen(combopattern) + 1) / 3) + 1;
	char* pattern = new char[patternLen];
	char* mask = new char[patternLen];

	Parse(combopattern, pattern, mask);

	char* match = AllMods(pattern, mask, process);

	delete[] pattern;
	delete[] mask;
	return match;
}

//Combo External Module
char* Pattern::Ex::Mod(char* combopattern, Module* module)
{
	unsigned int patternLen = ((strlen(combopattern) + 1) / 3) + 1;
	char* pattern = new char[patternLen];
	char* mask = new char[patternLen];

	Parse(combopattern, pattern, mask);

	char* match = Pattern::Ex::Mod(pattern, mask, module);

	delete[] pattern;
	delete[] mask;
	return match;
}

//Scan entire process
char* Pattern::Ex::Proc(char* combopattern, Process* process)
{
	unsigned int patternLen = ((strlen(combopattern) + 1) / 3) + 1;
	char* pattern = new char[patternLen];
	char* mask = new char[patternLen];
	Parse(combopattern, pattern, mask);


	process->GetBit();

	char* kernelMemory = nullptr;

	if (process->b32bit)
	{
		kernelMemory = (char*)0x80000000;
	}

	else
	{
		kernelMemory = (char*)0x00007fffffffffff;
	}


	char* match = Scan(pattern, mask, (char*)0x0, kernelMemory, process);

	delete[] pattern;
	delete[] mask;
	return match;
}

