#include "debug.h"
#include <iostream>

//External
bool Debug::IsDebugFlagSet(Process* process)
{
	process->GetPEB();

	if (process->peb.BeingDebugged == 1)
	{
		return true;
	}
	else return false;
}

bool Debug::SetDebugFlagFalse(Process* process)
{
	PatchEx((char*)process->pbi.PebBaseAddress + 0x2, "\x0", 1, process->handle);

	process->GetBit();

	//if process is running under Wow64
	if (process->bWow64)
	{
		//Write other PEB also
		PatchEx((char*)process->pbi.PebBaseAddress - 0x1000 + 0x2, "\x0", 1, process->handle);
	}
	//else return true;
	return true;
}

//internal only
bool Debug::PatchIsDebuggerPresent(Process* process)
{
	Module kernel32 = Module(process, TEXT("KERNEL32.DLL"));
	char* functionAddress = (char*)GetProcAddress(kernel32.hModule, "IsDebuggerPresent");

	//xor eax, eax; return near, nop nop
	Patch(functionAddress, "\x31\xC0\xC3\x90\x90", 5);
	SetDebugFlagFalse(process);

	return true;
}

bool Debug::HookIsDebuggerPresent(Process* process)
{
	//placeholder
	return true;
}

void Debug::TutorialTestPatchDebugger()
{
	Process thisExe = Process(TEXT("PatternScanExTutorial.exe"));
	thisExe.Attach();
	thisExe.GetPEB();

	BOOL intDebugPresent = 0;

	//Internal Process
	CheckRemoteDebuggerPresent(thisExe.handle, &intDebugPresent);
	intDebugPresent = IsDebuggerPresent();

	std::cout << "Before:\nIsDebuggerPresent = ";
	if (intDebugPresent == 0)
	{
		std::cout << "FALSE - GH r0x0rz y00 b0x0rs\n\n";
	}
	else std::cout << "TRUE - ANTI CHEAT ACTIVATED \n\n";

	Debug::PatchIsDebuggerPresent(&thisExe);

	CheckRemoteDebuggerPresent(thisExe.handle, &intDebugPresent);
	intDebugPresent = IsDebuggerPresent();

	std::cout << "After:\nIsDebuggerPresent = ";
	if (intDebugPresent == 0)
	{
		std::cout << "FALSE - GH r0x0rz y00 b0x0rs\n\n";
	}
	else std::cout << "TRUE - ANTI CHEAT ACTIVATED \n\n";
}