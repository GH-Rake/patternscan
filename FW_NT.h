#pragma once
#include <windows.h>
#include <winternl.h>
//From http://undocumented.ntinternals.net/

struct FWNT_PEB_LDR_DATA
{
	ULONG                   Length;
	BOOLEAN                 Initialized;
	PVOID                   SsHandle;
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
};// PEB_LDR_DATA, *PPEB_LDR_DATA;

struct FWNT_LDR_DATA_TABLE_ENTRY
{
	LIST_ENTRY InLoadOrderLinks; /* 0x00 */
	LIST_ENTRY InMemoryOrderLinks; /* 0x08 */
	LIST_ENTRY InInitializationOrderLinks; /* 0x10 */
	PVOID DllBase; /* 0x18 */
	PVOID EntryPoint;
	ULONG SizeOfImage;
	UNICODE_STRING FullDllName; /* 0x24 */
	UNICODE_STRING BaseDllName;
	ULONG Flags;
	WORD LoadCount;
	WORD TlsIndex;
	union
	{
		LIST_ENTRY HashLinks;
		struct
		{
			PVOID SectionPointer;
			ULONG CheckSum;
		};
	};
	union
	{
		ULONG TimeDateStamp;
		PVOID LoadedImports;
	};
	_ACTIVATION_CONTEXT * EntryPointActivationContext;
	PVOID PatchInformation;
	LIST_ENTRY ForwarderLinks;
	LIST_ENTRY ServiceTagLinks;
	LIST_ENTRY StaticLinks;
}; //LDR_DATA_TABLE_ENTRY, *PLDR_DATA_TABLE_ENTRY;

union FWNT_UNION_LDR_DATA_TABLE_ENTRY
{
	LIST_ENTRY list;
	FWNT_LDR_DATA_TABLE_ENTRY dataEntry;
};

//or we can just change the first to vars of FWNT_LDR_DATA_TABLE_ENTRY to:
/*
LIST_ENTRY* Flink;
LIST_ENTRY* Blink
*/

struct FW_LDR_MODULE {
	LIST_ENTRY              InLoadOrderModuleList;
	LIST_ENTRY              InMemoryOrderModuleList;
	LIST_ENTRY              InInitializationOrderModuleList;
	PVOID                   BaseAddress;
	PVOID                   EntryPoint;
	ULONG                   SizeOfImage;
	UNICODE_STRING          FullDllName;
	UNICODE_STRING          BaseDllName;
	ULONG                   Flags;
	SHORT                   LoadCount;
	SHORT                   TlsIndex;
	LIST_ENTRY              HashTableEntry;
	ULONG                   TimeDateStamp;
};