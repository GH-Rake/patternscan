#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include <Winternl.h>
#include <stdlib.h>
#include <tchar.h>
#include <vector>
#include "memhack.h"
#include <winnt.h>

class Module;

class ProcessIn
{
public:
	PEB* peb;

	bool b32bit = false;
	bool bWow64 = false;

	//Defines process as 32/64 bit
	bool GetBit();

	bool GetPEB();
};

class ProcessEx : public PROCESSENTRY32
{
public:
	TCHAR* name;
	HANDLE handle = 0;
	bool bFound = false;

	bool b32bit = false;
	bool bWow64 = false;

	PROCESS_BASIC_INFORMATION pbi;
	PEB peb = { 0 };

	//Constructor
	ProcessEx(TCHAR* exeName);

	//Finds process and get the procEntry
	bool Get();

	//Gets Handle to process
	bool Attach();

	//Defines process as 32/64 bit
	void GetBit();

	//Use NTQueryInformation to get the PEB address and make a copy of it
	bool GetPEB();

	bool GetModuleFromPEB(wchar_t* moduleName); //argument doesn't belong?
};

class Module : public MODULEENTRY32
{
public:
	TCHAR* name;
	bool bFound = false;
	ProcessEx* process; //pointer to parent process

	ProcessIn* processin; //pointer to parent process

	LDR_DATA_TABLE_ENTRY ldr; //?

	//Constructor
	Module(ProcessEx* process, TCHAR* moduleName);

	//Find module and get the modEntry
	bool Get();

	bool GetLDREntryIn();
};

//add this in there somehow
//https://www.codeproject.com/articles/320748/haephrati-elevating-during-runtime
BOOL IsAppRunningAsAdminMode();
void Elevate();