# PatternScan

My first git project, my mum would be so proud.

## What does this do?

This is an example project using a small library of process/module interaction functions and internal and external pattern functions that are part of my larger Hack framework.

## Why?

Playing with some new code I've been working on.  *Sometimes* pattern scanning is better than pointers.

## Usage

Launch Assault Cube.  Compile project.  Set a breakpoint on the first line.  Step through every line and witness the magic.

## TODO

Finish internal family of functions
Separate internal and external functions somehow to only be included if you're building an internal or external hack.  Would be nice to just use 

```
#!c++

#ifdef FW_EXTERNAL
#include "FW\external\patternscan.h"
#else
#include "FW\internal\patternscan.h"
#endif
```

## Development History

Originally [Fleep had made an Internal Pattern Scan Tutorial](http://guidedhacking.com/showthread.php?3981-C-Signature-Scan-Pattern-Scanning-Tutorial-DIFFICULTY-3-10)

[Fleep Video](
https://youtu.be/mKUSLJjlajg)

Initially I made an [External version](http://guidedhacking.com/showthread.php?8255-C-External-Signature-Scanning) of his Tutorial to compliment his tutorial.

[Rake Video](https://youtu.be/jLfPdujSuRA)

I have been evolving the functionality of this project, gearing up for part 2 of this video series.  It contains code intended to showcase tips and tricks I have to share that may not relate to the core purpose of this project.

##Credits

Shoutout to my friend Solaire, some of this code inspired by his holiness.
And thank you to ALL my friends at GuidedHacking